## Table of Content

[[_TOC_]]

## Introduction
This app developed with *Laravel Web Framework* and *Core UI Admin Dashboard*.

This app mean to be achieve both of the *mandatory phase* and the *challenge phase*. Here are the feature list :
* Login and Register using Student ID
* Buy and Sell Product on the store
* Add and Take some amount of money from Money Box
* Only logged in student (and admin) that may to buy/sell product and add/take money in money box

## Credential Info
Log in into the app as admin using following credential :
```
Username : admin
Password : password
```

## Feature Explanation
### 1. Generate Student ID
First, the app will generate the Student ID list along with their generated random name and store it into `students` table in database after `php artisan migrate:refresh --seed` command.

The generate Student ID function is stored in `database/factories/StudentFactory.php` file. Here's the code :

``` php
// StudentFactory.php
...

public function generateStudentId()
    {  
        // generate 3 random digit number, and turn it into array 
        $three_num =  $this->faker->unique()->randomNumber($nbDigits = 3, $strict = true);
        $three_num = strval($three_num);
        $three_array = str_split($three_num);

        // sum up the three number
        $three_sum = 0;
        foreach ($three_array as $item) {
            $three_sum += (int) $item;
        }
        
        // add zero if the sum number only have one digit
        $three_sum = (string) $three_sum;
        if(strlen($three_sum) == 1 ){
            $three_sum = '0' . $three_sum;
        }

        // concate the three random digit with the sum digit
        $student_id = $three_num . $three_sum;

        return $student_id;
    }
}

...

```

<sub>Note : The generated Student ID List can be viewed from `http://[app-domain]/student-list` (only admin can view the page). </sub>

### 2. Validating Student ID on Register
When someone is trying to register, the app will check if the inputted `student_id` is exist on `student` table and not has been registered yet.

The function is stored in `app/Http/Controllers/CustomRegisterController.php` file. Here's the code :
``` php
// CustomRegisterController.php

...

public function handle()
    {
        request()->validate([
            'student_id' => ['required', 'string', 'max:255', 'exists:students,student_id'],
            'email' => ['required', 'email', 'max:255'],
            'password' => ['required', 'string', 'min:8', 'confirmed']
        ]);

        $student = Student::where('student_id', '=', request('student_id'))->first();

        // dd($student->student_id);
        
        if ($student != null) {
            // execute if student id not used
            if(is_null($student->register_status)){
                
                $user =  User::create([
                    'name' => $student->student_name,
                    'email' => request('email'),
                    'password' => Hash::make(request('password')),
                    'student_id' => $student->student_id,
                ]);
    
                $student->register_status = "registered";
                $student->save();
    
                $user->assignRole('user');
                
                Auth::login($user);

                return redirect()->route('product.index');

            }else{
                return redirect()->route('register')
                    ->with('error', 'Sorry, Student ID has been used.');
            }
        }
    }

...

```

<sub>Note : When registering, student *not necessary to input their name*. The app will get their name that stored in the `student` table.</sub>

Then, in order to test the feature, one's need to get one of student id (copy it from the Student List Page), and do registration on the Register Page (`http://[app-domain]/register`)

After registered, admin can see and manage the new registered account on *User page* (`http://[app-domain]/users`).



## Installation

### Installing the Dependencies
There's two ways to get it done. First method is use `master branch` and then manually install the dependencies, second method is switching to `full_dir branch` which has dependencies included.

<sub>Note : In previous case, `composer install` command sometimes generating some error. <br> So i decide to push the whole directory to `full_dir branch` so the client not necessary to run the command in the *second method*.</sub>

#### 1. First Method : Manually Install Dependencies

``` bash
# clone the repo
$ git clone https://gitlab.com/sakanario/kantin.git kantin

# go into app's directory
$ cd kantin

# install app's dependencies
$ composer install

# install app's dependencies
$ npm install

```

#### 2. Second Method : Switching to full_dir branch
``` bash
# clone the repo
$ git clone https://gitlab.com/sakanario/kantin.git kantin

# go into app's directory
$ cd kantin

# switching to full_dir branch
$ git checkout full_dir

```

### Setup Database 
#### MySQL
In file ".env" complete this database configuration:
* DB_CONNECTION=mysql
* DB_HOST=127.0.0.1
* DB_PORT=3306
* DB_DATABASE=kantin12
* DB_USERNAME=root
* DB_PASSWORD=

#### PostgreSQL
1. Install PostgreSQL

2. Create user

``` bash
$ sudo -u postgres createuser --interactive
enter name of role to add: laravel
shall the new role be a superuser (y/n) n
shall the new role be allowed to create database (y/n) n
shall the new role be allowed to create more new roles (y/n) n
```
3. Set user password
``` bash
$ sudo -u postgres psql
postgres= ALTER USER laravel WITH ENCRYPTED PASSWORD 'password';
postgres= \q
```

4. Create database
```
$ sudo -u postgres createdb laravel
```
5. Then in file ".env" replace this database configuration:

* DB_CONNECTION=mysql
* DB_HOST=127.0.0.1
* DB_PORT=3306
* DB_DATABASE=laravel
* DB_USERNAME=root
* DB_PASSWORD=

To this:

* DB_CONNECTION=pgsql
* DB_HOST=127.0.0.1
* DB_PORT=5432
* DB_DATABASE=laravel
* DB_USERNAME=laravel
* DB_PASSWORD=password

<sub>Note : `.env` file already included on the repo and has the above value.</sub>


### "Warm The Engine"

``` bash
# in your app directory
# generate laravel APP_KEY
$ php artisan key:generate

# run database migration and seed
$ php artisan migrate:refresh --seed

# generate mixing
$ npm run dev

# and repeat generate mixing
$ npm run dev
```

### Run The App!

``` bash
# start local server
$ php artisan serve

```

Open the browser with address: [http://localhost:8000](localhost:8000)  
Click "Login" on nav menu and log in with following credential:

* Usename: "admin"
* Password: "password"

## Developer Note
Thanks for the reviewer for evaluating this repo. I have been soo committed to this project in these past day.  Yes, there's so many part that i supposed to do better. I really apreciate for any suggestion or critic. 

Thankyou

Regards, <br>
Satria Laksana 


<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class StudentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'student_id' => $this->generateStudentId(),
            'student_name' => $this->faker->name(),
        ];
    }

    public function generateStudentId()
    {  
        // generate 3 random digit number, and turn it into array 
        $three_num =  $this->faker->unique()->randomNumber($nbDigits = 3, $strict = true);
        $three_num = strval($three_num);
        $three_array = str_split($three_num);

        // sum up the three number
        $three_sum = 0;
        foreach ($three_array as $item) {
            $three_sum += (int) $item;
        }
        
        // add zero if the sum number only have one digit
        $three_sum = (string) $three_sum;
        if(strlen($three_sum) == 1 ){
            $three_sum = '0' . $three_sum;
        }

        // concate the three random digit with the sum digit
        $student_id = $three_num . $three_sum;

        return $student_id;
    }
}

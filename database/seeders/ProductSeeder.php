<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'product_name' => "Kue Bolu",
            'product_price' => 15000,
            'product_description' => "Kue bolu adalah kue berbahan dasar tepung (umumnya tepung terigu, gula, telur)",
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'product_image' => "kue-bolu.jpg",
        ]);

        DB::table('products')->insert([
            'product_name' => "Onigiri",
            'product_price' => 20000,
            'product_description' => "Onigiri adalah nama Jepang untuk makanan berupa nasi yang dipadatkan sewaktu masih hangat sehingga berbentuk segitiga, bulat, atau seperti karung beras.",
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'product_image' => "onigiri.jpg",
        ]);

        DB::table('products')->insert([
            'product_name' => "Kimchi",
            'product_price' => 30000,
            'product_description' => "Kimci adalah makanan tradisional Korea berupa asinan sayur hasil fermentasi yang diberi bumbu pedas. Setelah digarami dan dicuci, sayuran dicampur dengan bumbu",
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'product_image' => "Kimchi.jpg",
        ]);

        DB::table('products')->insert([
            'product_name' => "Ramen",
            'product_price' => 25000,
            'product_description' => "Ramen adalah salah satu olahan makanan khas negara Jepang yang terbuat dari bahan dasar berupa mie yang berkuah.",
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'product_image' => "ramen.jpeg",
        ]);
    }
}

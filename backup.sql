--
-- PostgreSQL database dump
--

-- Dumped from database version 14.4 (Ubuntu 14.4-0ubuntu0.22.04.1)
-- Dumped by pg_dump version 14.4 (Ubuntu 14.4-0ubuntu0.22.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: celengans; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.celengans (
    id bigint NOT NULL,
    saldo numeric(13,2) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


--
-- Name: celengans_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.celengans_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: celengans_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.celengans_id_seq OWNED BY public.celengans.id;


--
-- Name: email_template; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.email_template (
    id bigint NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    content text NOT NULL,
    name character varying(255) NOT NULL,
    subject character varying(255) NOT NULL
);


--
-- Name: email_template_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.email_template_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: email_template_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.email_template_id_seq OWNED BY public.email_template.id;


--
-- Name: example; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.example (
    id bigint NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    name character varying(255) NOT NULL,
    description character varying(255) NOT NULL,
    status_id integer NOT NULL
);


--
-- Name: example_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.example_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: example_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.example_id_seq OWNED BY public.example.id;


--
-- Name: failed_jobs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.failed_jobs (
    id bigint NOT NULL,
    connection text NOT NULL,
    queue text NOT NULL,
    payload text NOT NULL,
    exception text NOT NULL,
    failed_at timestamp(0) without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


--
-- Name: failed_jobs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.failed_jobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: failed_jobs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.failed_jobs_id_seq OWNED BY public.failed_jobs.id;


--
-- Name: folder; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.folder (
    id bigint NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    name character varying(255) NOT NULL,
    folder_id integer,
    resource boolean
);


--
-- Name: folder_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.folder_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: folder_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.folder_id_seq OWNED BY public.folder.id;


--
-- Name: form; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.form (
    id bigint NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    name character varying(255) NOT NULL,
    table_name character varying(255) NOT NULL,
    read boolean NOT NULL,
    edit boolean NOT NULL,
    add boolean NOT NULL,
    delete boolean NOT NULL,
    pagination integer NOT NULL
);


--
-- Name: form_field; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.form_field (
    id bigint NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    name character varying(255) NOT NULL,
    type character varying(255) NOT NULL,
    browse boolean NOT NULL,
    read boolean NOT NULL,
    edit boolean NOT NULL,
    add boolean NOT NULL,
    relation_table character varying(255),
    relation_column character varying(255),
    form_id integer NOT NULL,
    column_name character varying(255) NOT NULL
);


--
-- Name: form_field_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.form_field_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: form_field_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.form_field_id_seq OWNED BY public.form_field.id;


--
-- Name: form_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.form_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: form_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.form_id_seq OWNED BY public.form.id;


--
-- Name: media; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.media (
    id bigint NOT NULL,
    model_type character varying(255) NOT NULL,
    model_id bigint NOT NULL,
    collection_name character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    file_name character varying(255) NOT NULL,
    mime_type character varying(255),
    disk character varying(255) NOT NULL,
    conversions_disk character varying(255) NOT NULL,
    size bigint NOT NULL,
    uuid bigint NOT NULL,
    manipulations json NOT NULL,
    custom_properties json NOT NULL,
    responsive_images json NOT NULL,
    order_column integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


--
-- Name: media_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.media_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: media_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.media_id_seq OWNED BY public.media.id;


--
-- Name: menu_role; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.menu_role (
    id bigint NOT NULL,
    role_name character varying(255) NOT NULL,
    menus_id integer NOT NULL
);


--
-- Name: menu_role_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.menu_role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: menu_role_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.menu_role_id_seq OWNED BY public.menu_role.id;


--
-- Name: menulist; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.menulist (
    id bigint NOT NULL,
    name character varying(255) NOT NULL
);


--
-- Name: menulist_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.menulist_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: menulist_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.menulist_id_seq OWNED BY public.menulist.id;


--
-- Name: menus; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.menus (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    href character varying(255),
    icon character varying(255),
    slug character varying(255) NOT NULL,
    parent_id integer,
    menu_id integer NOT NULL,
    sequence integer NOT NULL
);


--
-- Name: menus_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.menus_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: menus_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.menus_id_seq OWNED BY public.menus.id;


--
-- Name: migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.migrations (
    id integer NOT NULL,
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);


--
-- Name: migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.migrations_id_seq OWNED BY public.migrations.id;


--
-- Name: model_has_permissions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.model_has_permissions (
    permission_id bigint NOT NULL,
    model_type character varying(255) NOT NULL,
    model_id bigint NOT NULL
);


--
-- Name: model_has_roles; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.model_has_roles (
    role_id bigint NOT NULL,
    model_type character varying(255) NOT NULL,
    model_id bigint NOT NULL
);


--
-- Name: notes; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.notes (
    id bigint NOT NULL,
    title character varying(255) NOT NULL,
    content text NOT NULL,
    note_type character varying(255) NOT NULL,
    applies_to_date date NOT NULL,
    users_id integer NOT NULL,
    status_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


--
-- Name: notes_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.notes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: notes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.notes_id_seq OWNED BY public.notes.id;


--
-- Name: password_resets; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.password_resets (
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    created_at timestamp(0) without time zone
);


--
-- Name: permissions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.permissions (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    guard_name character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


--
-- Name: permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.permissions_id_seq OWNED BY public.permissions.id;


--
-- Name: products; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.products (
    id bigint NOT NULL,
    product_name character varying(255) NOT NULL,
    product_price integer NOT NULL,
    product_description text NOT NULL,
    product_image text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


--
-- Name: products_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.products_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: products_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.products_id_seq OWNED BY public.products.id;


--
-- Name: role_has_permissions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.role_has_permissions (
    permission_id bigint NOT NULL,
    role_id bigint NOT NULL
);


--
-- Name: role_hierarchy; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.role_hierarchy (
    id bigint NOT NULL,
    role_id integer NOT NULL,
    hierarchy integer NOT NULL
);


--
-- Name: role_hierarchy_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.role_hierarchy_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: role_hierarchy_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.role_hierarchy_id_seq OWNED BY public.role_hierarchy.id;


--
-- Name: roles; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.roles (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    guard_name character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


--
-- Name: roles_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.roles_id_seq OWNED BY public.roles.id;


--
-- Name: status; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.status (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    class character varying(255) NOT NULL
);


--
-- Name: status_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.status_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: status_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.status_id_seq OWNED BY public.status.id;


--
-- Name: students; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.students (
    student_id character varying(255) NOT NULL,
    student_name character varying(255) NOT NULL,
    register_status character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


--
-- Name: users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.users (
    id bigint NOT NULL,
    student_id character varying(255),
    name character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    email_verified_at timestamp(0) without time zone,
    password character varying(255) NOT NULL,
    menuroles character varying(255) NOT NULL,
    remember_token character varying(100),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: celengans id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.celengans ALTER COLUMN id SET DEFAULT nextval('public.celengans_id_seq'::regclass);


--
-- Name: email_template id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.email_template ALTER COLUMN id SET DEFAULT nextval('public.email_template_id_seq'::regclass);


--
-- Name: example id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.example ALTER COLUMN id SET DEFAULT nextval('public.example_id_seq'::regclass);


--
-- Name: failed_jobs id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.failed_jobs ALTER COLUMN id SET DEFAULT nextval('public.failed_jobs_id_seq'::regclass);


--
-- Name: folder id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.folder ALTER COLUMN id SET DEFAULT nextval('public.folder_id_seq'::regclass);


--
-- Name: form id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.form ALTER COLUMN id SET DEFAULT nextval('public.form_id_seq'::regclass);


--
-- Name: form_field id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.form_field ALTER COLUMN id SET DEFAULT nextval('public.form_field_id_seq'::regclass);


--
-- Name: media id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.media ALTER COLUMN id SET DEFAULT nextval('public.media_id_seq'::regclass);


--
-- Name: menu_role id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.menu_role ALTER COLUMN id SET DEFAULT nextval('public.menu_role_id_seq'::regclass);


--
-- Name: menulist id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.menulist ALTER COLUMN id SET DEFAULT nextval('public.menulist_id_seq'::regclass);


--
-- Name: menus id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.menus ALTER COLUMN id SET DEFAULT nextval('public.menus_id_seq'::regclass);


--
-- Name: migrations id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.migrations ALTER COLUMN id SET DEFAULT nextval('public.migrations_id_seq'::regclass);


--
-- Name: notes id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.notes ALTER COLUMN id SET DEFAULT nextval('public.notes_id_seq'::regclass);


--
-- Name: permissions id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.permissions ALTER COLUMN id SET DEFAULT nextval('public.permissions_id_seq'::regclass);


--
-- Name: products id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.products ALTER COLUMN id SET DEFAULT nextval('public.products_id_seq'::regclass);


--
-- Name: role_hierarchy id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.role_hierarchy ALTER COLUMN id SET DEFAULT nextval('public.role_hierarchy_id_seq'::regclass);


--
-- Name: roles id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.roles ALTER COLUMN id SET DEFAULT nextval('public.roles_id_seq'::regclass);


--
-- Name: status id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.status ALTER COLUMN id SET DEFAULT nextval('public.status_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Data for Name: celengans; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.celengans (id, saldo, created_at, updated_at) FROM stdin;
1	0.00	\N	\N
\.


--
-- Data for Name: email_template; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.email_template (id, created_at, updated_at, content, name, subject) FROM stdin;
1	\N	\N	<!DOCTYPE html>\n                <html lang="en">\n                <head>\n                    <meta charset="utf-8">\n                    <meta name="viewport" content="width=device-width">\n                    <meta http-equiv="X-UA-Compatible" content="IE=edge"> \n                    <meta name="x-apple-disable-message-reformatting">\n                    <title>Example</title>\n                    <style>\n                        body {\n                            background-color:#fff;\n                            color:#222222;\n                            margin: 0px auto;\n                            padding: 0px;\n                            height: 100%;\n                            width: 100%;\n                            font-weight: 400;\n                            font-size: 15px;\n                            line-height: 1.8;\n                        }\n                        .continer{\n                            width:400px;\n                            margin-left:auto;\n                            margin-right:auto;\n                            background-color:#efefef;\n                            padding:30px;\n                        }\n                        .btn{\n                            padding: 5px 15px;\n                            display: inline-block;\n                        }\n                        .btn-primary{\n                            border-radius: 3px;\n                            background: #0b3c7c;\n                            color: #fff;\n                            text-decoration: none;\n                        }\n                        .btn-primary:hover{\n                            border-radius: 3px;\n                            background: #4673ad;\n                            color: #fff;\n                            text-decoration: none;\n                        }\n                    </style>\n                </head>\n                <body>\n                    <div class="continer">\n                        <h1>Lorem ipsum dolor</h1>\n                        <h4>Ipsum dolor cet emit amet</h4>\n                        <p>\n                            Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea <strong>commodo consequat</strong>. \n                            Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. \n                            Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\n                        </p>\n                        <h4>Ipsum dolor cet emit amet</h4>\n                        <p>\n                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod <a href="#">tempor incididunt ut labore</a> et dolore magna aliqua.\n                            Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. \n                        </p>\n                        <h4>Ipsum dolor cet emit amet</h4>\n                        <p>\n                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\n                            Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. \n                        </p>\n                        <a href="#" class="btn btn-primary">Lorem ipsum dolor</a>\n                        <h4>Ipsum dolor cet emit amet</h4>\n                        <p>\n                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\n                            Ut enim ad minim veniam, quis nostrud exercitation <a href="#">ullamco</a> laboris nisi ut aliquip ex ea commodo consequat. \n                            Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. \n                            Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\n                        </p>\n                    </div>\n                </body>\n                </html>	Example E-mail	Example E-mail
\.


--
-- Data for Name: example; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.example (id, created_at, updated_at, name, description, status_id) FROM stdin;
1	\N	\N	Dolor esse omnis praesentium.	Beatae sit vero voluptatibus accusamus.	2
2	\N	\N	Reprehenderit fuga quos.	Beatae laborum officia voluptatum aspernatur.	3
3	\N	\N	Aliquam neque non.	Magni dolorem vel nesciunt rerum. Sequi blanditiis repudiandae vero voluptatem sapiente.	1
4	\N	\N	Culpa natus ipsam natus voluptas.	Et ducimus in ex amet reprehenderit consequatur.	3
5	\N	\N	Quisquam eius illo.	Aut harum rem voluptatum sit.	3
6	\N	\N	Et vitae consequatur.	Ad consequuntur et magnam.	2
7	\N	\N	Est consequatur modi nostrum.	Dolorem consequatur delectus magni dolores quasi hic totam.	4
8	\N	\N	Laborum tempora nobis odit.	Et explicabo rerum et repudiandae perferendis sed.	4
9	\N	\N	In expedita rerum consequatur quibusdam.	Saepe in quidem minima ipsum aut consequatur molestiae. Veritatis libero suscipit et laboriosam tempora similique.	1
10	\N	\N	Facere aut necessitatibus voluptatem.	Porro sunt ipsa repellat autem non impedit vero non.	3
11	\N	\N	Quaerat earum nesciunt ipsam cumque et.	Vel cumque omnis ut nobis corporis deserunt. Consequatur sit qui ad voluptates.	2
12	\N	\N	Vero asperiores enim.	Voluptatem et ut id ad animi maxime. Soluta ullam vel voluptatum animi libero.	1
13	\N	\N	Eum necessitatibus aut dolorem.	Dolorem maiores velit vel non eum aut.	3
14	\N	\N	Officiis atque et.	Consequuntur unde aut eligendi assumenda maxime. Nobis soluta sapiente tenetur nulla qui omnis.	1
15	\N	\N	Cum et amet esse commodi atque.	Ipsum iure id qui nesciunt enim fugit repudiandae. Doloremque exercitationem officiis quis quas.	2
16	\N	\N	Quo deserunt minus.	Non sit magnam expedita explicabo tempora nam fuga.	3
17	\N	\N	Et placeat voluptatem.	Nam vel assumenda voluptatem similique impedit.	3
18	\N	\N	Laborum omnis voluptatem dignissimos.	Et aut eum sunt odit. Et non debitis accusamus reprehenderit illo.	4
19	\N	\N	Dolorem ut iusto aut.	Ipsam molestiae id eos sunt. In placeat inventore et odio et ratione eveniet.	3
20	\N	\N	Ut qui provident.	Modi molestiae rerum at vitae ut saepe eum sed.	3
21	\N	\N	At aperiam voluptatem.	Dolorum nisi voluptas labore rem modi velit. Architecto perferendis eligendi odit veniam veniam voluptatem aut.	1
22	\N	\N	Nihil ut cumque libero enim dolore.	Atque expedita quam incidunt ab esse.	1
23	\N	\N	Aut qui eos est facilis.	Laudantium odio doloremque corporis rem adipisci impedit ut suscipit.	2
24	\N	\N	Id ducimus ea sint.	Vel culpa dolores in. Molestias repellat repellendus architecto.	2
25	\N	\N	Cum repellendus dolorem.	Veritatis minima porro nobis voluptate et.	2
\.


--
-- Data for Name: failed_jobs; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.failed_jobs (id, connection, queue, payload, exception, failed_at) FROM stdin;
\.


--
-- Data for Name: folder; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.folder (id, created_at, updated_at, name, folder_id, resource) FROM stdin;
1	\N	\N	root	\N	\N
2	\N	\N	resource	1	t
3	\N	\N	documents	1	\N
4	\N	\N	graphics	1	\N
5	\N	\N	other	1	\N
\.


--
-- Data for Name: form; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.form (id, created_at, updated_at, name, table_name, read, edit, add, delete, pagination) FROM stdin;
1	\N	\N	Example	example	t	t	t	t	5
\.


--
-- Data for Name: form_field; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.form_field (id, created_at, updated_at, name, type, browse, read, edit, add, relation_table, relation_column, form_id, column_name) FROM stdin;
1	\N	\N	Title	text	t	t	t	t	\N	\N	1	name
2	\N	\N	Description	text_area	t	t	t	t	\N	\N	1	description
3	\N	\N	Status	relation_select	t	t	t	t	status	name	1	status_id
\.


--
-- Data for Name: media; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.media (id, model_type, model_id, collection_name, name, file_name, mime_type, disk, conversions_disk, size, uuid, manipulations, custom_properties, responsive_images, order_column, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: menu_role; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.menu_role (id, role_name, menus_id) FROM stdin;
1	admin	1
2	admin	2
\.


--
-- Data for Name: menulist; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.menulist (id, name) FROM stdin;
1	sidebar menu
\.


--
-- Data for Name: menus; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.menus (id, name, href, icon, slug, parent_id, menu_id, sequence) FROM stdin;
1	Student List	/student-list	cil-list	link	\N	1	1
2	Users	/users	cil-smile	link	\N	1	2
\.


--
-- Data for Name: migrations; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.migrations (id, migration, batch) FROM stdin;
1	2014_10_12_000000_create_users_table	1
2	2014_10_12_100000_create_password_resets_table	1
3	2019_08_19_000000_create_failed_jobs_table	1
4	2019_10_11_085455_create_notes_table	1
5	2019_10_12_115248_create_status_table	1
6	2019_11_08_102827_create_menus_table	1
7	2019_11_13_092213_create_menurole_table	1
8	2019_12_10_092113_create_permission_tables	1
9	2019_12_11_091036_create_menulist_table	1
10	2019_12_18_092518_create_role_hierarchy_table	1
11	2020_01_07_093259_create_folder_table	1
12	2020_01_08_184500_create_media_table	1
13	2020_01_21_161241_create_form_field_table	1
14	2020_01_21_161242_create_form_table	1
15	2020_01_21_161243_create_example_table	1
16	2020_03_12_111400_create_email_template_table	1
17	2022_07_05_121830_create_products_table	1
18	2022_07_05_135141_create_celengans_table	1
19	2022_07_06_224518_create_students_table	1
\.


--
-- Data for Name: model_has_permissions; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.model_has_permissions (permission_id, model_type, model_id) FROM stdin;
\.


--
-- Data for Name: model_has_roles; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.model_has_roles (role_id, model_type, model_id) FROM stdin;
1	App\\Models\\User	1
2	App\\Models\\User	1
\.


--
-- Data for Name: notes; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.notes (id, title, content, note_type, applies_to_date, users_id, status_id, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: password_resets; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.password_resets (email, token, created_at) FROM stdin;
\.


--
-- Data for Name: permissions; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.permissions (id, name, guard_name, created_at, updated_at) FROM stdin;
1	browse bread 1	web	2022-07-08 22:17:18	2022-07-08 22:17:18
2	read bread 1	web	2022-07-08 22:17:18	2022-07-08 22:17:18
3	edit bread 1	web	2022-07-08 22:17:18	2022-07-08 22:17:18
4	add bread 1	web	2022-07-08 22:17:18	2022-07-08 22:17:18
5	delete bread 1	web	2022-07-08 22:17:18	2022-07-08 22:17:18
\.


--
-- Data for Name: products; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.products (id, product_name, product_price, product_description, product_image, created_at, updated_at) FROM stdin;
1	Kue Bolu	15000	Kue bolu adalah kue berbahan dasar tepung (umumnya tepung terigu, gula, telur)	kue-bolu.jpg	2022-07-08 22:17:19	\N
2	Onigiri	20000	Onigiri adalah nama Jepang untuk makanan berupa nasi yang dipadatkan sewaktu masih hangat sehingga berbentuk segitiga, bulat, atau seperti karung beras.	onigiri.jpg	2022-07-08 22:17:19	\N
3	Kimchi	30000	Kimci adalah makanan tradisional Korea berupa asinan sayur hasil fermentasi yang diberi bumbu pedas. Setelah digarami dan dicuci, sayuran dicampur dengan bumbu	Kimchi.jpg	2022-07-08 22:17:19	\N
4	Ramen	25000	Ramen adalah salah satu olahan makanan khas negara Jepang yang terbuat dari bahan dasar berupa mie yang berkuah.	ramen.jpeg	2022-07-08 22:17:19	\N
\.


--
-- Data for Name: role_has_permissions; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.role_has_permissions (permission_id, role_id) FROM stdin;
1	3
2	3
3	3
4	3
5	3
\.


--
-- Data for Name: role_hierarchy; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.role_hierarchy (id, role_id, hierarchy) FROM stdin;
1	1	1
2	2	2
3	3	3
\.


--
-- Data for Name: roles; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.roles (id, name, guard_name, created_at, updated_at) FROM stdin;
1	admin	web	2022-07-08 22:17:18	2022-07-08 22:17:18
2	user	web	2022-07-08 22:17:18	2022-07-08 22:17:18
3	guest	web	2022-07-08 22:17:18	2022-07-08 22:17:18
\.


--
-- Data for Name: status; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.status (id, name, class) FROM stdin;
1	ongoing	badge badge-pill badge-primary
2	stopped	badge badge-pill badge-secondary
3	completed	badge badge-pill badge-success
4	expired	badge badge-pill badge-warning
\.


--
-- Data for Name: students; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.students (student_id, student_name, register_status, created_at, updated_at) FROM stdin;
20305	Prof. Otho Jacobs DDS	\N	2022-07-08 22:17:19	2022-07-08 22:17:19
17917	Jettie Cronin	\N	2022-07-08 22:17:19	2022-07-08 22:17:19
51814	Prof. Coy Abernathy	\N	2022-07-08 22:17:19	2022-07-08 22:17:19
56718	Nestor White	\N	2022-07-08 22:17:19	2022-07-08 22:17:19
88925	Gay Gusikowski	\N	2022-07-08 22:17:19	2022-07-08 22:17:19
86014	Brandyn McClure	\N	2022-07-08 22:17:19	2022-07-08 22:17:19
17614	Clarabelle Goldner Sr.	\N	2022-07-08 22:17:19	2022-07-08 22:17:19
80109	Dayna Greenfelder	\N	2022-07-08 22:17:19	2022-07-08 22:17:19
45918	Aurelio Medhurst	\N	2022-07-08 22:17:19	2022-07-08 22:17:19
68216	Lonie Moen	\N	2022-07-08 22:17:19	2022-07-08 22:17:19
69318	Ms. Hallie Orn	\N	2022-07-08 22:17:19	2022-07-08 22:17:19
76619	Dr. Margie O'Hara	\N	2022-07-08 22:17:19	2022-07-08 22:17:19
95418	Lucas Gusikowski	\N	2022-07-08 22:17:19	2022-07-08 22:17:19
14409	Dr. Morton Torphy Jr.	\N	2022-07-08 22:17:19	2022-07-08 22:17:19
67215	Nicklaus Mraz	\N	2022-07-08 22:17:19	2022-07-08 22:17:19
69823	Art Sipes	\N	2022-07-08 22:17:19	2022-07-08 22:17:19
83819	Gustave Waelchi	\N	2022-07-08 22:17:19	2022-07-08 22:17:19
16310	Audreanne Simonis	\N	2022-07-08 22:17:19	2022-07-08 22:17:19
35614	Jailyn Mayer	\N	2022-07-08 22:17:19	2022-07-08 22:17:19
42107	Prof. Joseph Smitham	\N	2022-07-08 22:17:19	2022-07-08 22:17:19
75113	Ella Kris	\N	2022-07-08 22:17:19	2022-07-08 22:17:19
73414	Susana Graham	\N	2022-07-08 22:17:19	2022-07-08 22:17:19
55616	Dr. Rosalee Walsh PhD	\N	2022-07-08 22:17:19	2022-07-08 22:17:19
86418	Dalton West	\N	2022-07-08 22:17:19	2022-07-08 22:17:19
22105	Teresa Prosacco	\N	2022-07-08 22:17:19	2022-07-08 22:17:19
68519	Bernard Botsford	\N	2022-07-08 22:17:19	2022-07-08 22:17:19
91111	Alvah Keebler	\N	2022-07-08 22:17:19	2022-07-08 22:17:19
44917	Efren Marks	\N	2022-07-08 22:17:19	2022-07-08 22:17:19
68620	Dr. Danielle Hagenes II	\N	2022-07-08 22:17:19	2022-07-08 22:17:19
93416	Mrs. Mariam Wilkinson	\N	2022-07-08 22:17:19	2022-07-08 22:17:19
35412	Dr. Patrick Walsh DVM	\N	2022-07-08 22:17:19	2022-07-08 22:17:19
43209	Jett Schumm DDS	\N	2022-07-08 22:17:19	2022-07-08 22:17:19
29516	Luis Rau	\N	2022-07-08 22:17:19	2022-07-08 22:17:19
73010	Ms. Abbey Smith	\N	2022-07-08 22:17:19	2022-07-08 22:17:19
17008	Bernie Glover PhD	\N	2022-07-08 22:17:19	2022-07-08 22:17:19
36211	Arthur Oberbrunner	\N	2022-07-08 22:17:19	2022-07-08 22:17:19
99725	Kennedy Jenkins	\N	2022-07-08 22:17:19	2022-07-08 22:17:19
52613	Chyna Wuckert	\N	2022-07-08 22:17:19	2022-07-08 22:17:19
74314	Dr. Kari Sporer	\N	2022-07-08 22:17:19	2022-07-08 22:17:19
10304	Guadalupe Gutkowski	\N	2022-07-08 22:17:19	2022-07-08 22:17:19
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.users (id, student_id, name, email, email_verified_at, password, menuroles, remember_token, created_at, updated_at, deleted_at) FROM stdin;
1	admin	admin	admin@admin.com	2022-07-08 22:17:18	$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi	user,admin	Nwg2UDsUGt	2022-07-08 22:17:18	2022-07-08 22:17:18	\N
\.


--
-- Name: celengans_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.celengans_id_seq', 1, true);


--
-- Name: email_template_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.email_template_id_seq', 1, true);


--
-- Name: example_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.example_id_seq', 25, true);


--
-- Name: failed_jobs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.failed_jobs_id_seq', 1, false);


--
-- Name: folder_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.folder_id_seq', 5, true);


--
-- Name: form_field_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.form_field_id_seq', 3, true);


--
-- Name: form_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.form_id_seq', 1, true);


--
-- Name: media_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.media_id_seq', 1, false);


--
-- Name: menu_role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.menu_role_id_seq', 2, true);


--
-- Name: menulist_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.menulist_id_seq', 1, true);


--
-- Name: menus_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.menus_id_seq', 2, true);


--
-- Name: migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.migrations_id_seq', 19, true);


--
-- Name: notes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.notes_id_seq', 1, false);


--
-- Name: permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.permissions_id_seq', 5, true);


--
-- Name: products_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.products_id_seq', 5, true);


--
-- Name: role_hierarchy_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.role_hierarchy_id_seq', 3, true);


--
-- Name: roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.roles_id_seq', 3, true);


--
-- Name: status_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.status_id_seq', 4, true);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.users_id_seq', 1, true);


--
-- Name: celengans celengans_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.celengans
    ADD CONSTRAINT celengans_pkey PRIMARY KEY (id);


--
-- Name: email_template email_template_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.email_template
    ADD CONSTRAINT email_template_pkey PRIMARY KEY (id);


--
-- Name: example example_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.example
    ADD CONSTRAINT example_pkey PRIMARY KEY (id);


--
-- Name: failed_jobs failed_jobs_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.failed_jobs
    ADD CONSTRAINT failed_jobs_pkey PRIMARY KEY (id);


--
-- Name: folder folder_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.folder
    ADD CONSTRAINT folder_pkey PRIMARY KEY (id);


--
-- Name: form_field form_field_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.form_field
    ADD CONSTRAINT form_field_pkey PRIMARY KEY (id);


--
-- Name: form form_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.form
    ADD CONSTRAINT form_pkey PRIMARY KEY (id);


--
-- Name: media media_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.media
    ADD CONSTRAINT media_pkey PRIMARY KEY (id);


--
-- Name: menu_role menu_role_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.menu_role
    ADD CONSTRAINT menu_role_pkey PRIMARY KEY (id);


--
-- Name: menulist menulist_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.menulist
    ADD CONSTRAINT menulist_pkey PRIMARY KEY (id);


--
-- Name: menus menus_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.menus
    ADD CONSTRAINT menus_pkey PRIMARY KEY (id);


--
-- Name: migrations migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);


--
-- Name: model_has_permissions model_has_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.model_has_permissions
    ADD CONSTRAINT model_has_permissions_pkey PRIMARY KEY (permission_id, model_id, model_type);


--
-- Name: model_has_roles model_has_roles_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.model_has_roles
    ADD CONSTRAINT model_has_roles_pkey PRIMARY KEY (role_id, model_id, model_type);


--
-- Name: notes notes_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.notes
    ADD CONSTRAINT notes_pkey PRIMARY KEY (id);


--
-- Name: permissions permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.permissions
    ADD CONSTRAINT permissions_pkey PRIMARY KEY (id);


--
-- Name: products products_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.products
    ADD CONSTRAINT products_pkey PRIMARY KEY (id);


--
-- Name: role_has_permissions role_has_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.role_has_permissions
    ADD CONSTRAINT role_has_permissions_pkey PRIMARY KEY (permission_id, role_id);


--
-- Name: role_hierarchy role_hierarchy_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.role_hierarchy
    ADD CONSTRAINT role_hierarchy_pkey PRIMARY KEY (id);


--
-- Name: roles roles_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);


--
-- Name: status status_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.status
    ADD CONSTRAINT status_pkey PRIMARY KEY (id);


--
-- Name: students students_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.students
    ADD CONSTRAINT students_pkey PRIMARY KEY (student_id);


--
-- Name: users users_email_unique; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_email_unique UNIQUE (email);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: media_model_type_model_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX media_model_type_model_id_index ON public.media USING btree (model_type, model_id);


--
-- Name: model_has_permissions_model_id_model_type_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX model_has_permissions_model_id_model_type_index ON public.model_has_permissions USING btree (model_id, model_type);


--
-- Name: model_has_roles_model_id_model_type_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX model_has_roles_model_id_model_type_index ON public.model_has_roles USING btree (model_id, model_type);


--
-- Name: password_resets_email_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX password_resets_email_index ON public.password_resets USING btree (email);


--
-- Name: model_has_permissions model_has_permissions_permission_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.model_has_permissions
    ADD CONSTRAINT model_has_permissions_permission_id_foreign FOREIGN KEY (permission_id) REFERENCES public.permissions(id) ON DELETE CASCADE;


--
-- Name: model_has_roles model_has_roles_role_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.model_has_roles
    ADD CONSTRAINT model_has_roles_role_id_foreign FOREIGN KEY (role_id) REFERENCES public.roles(id) ON DELETE CASCADE;


--
-- Name: role_has_permissions role_has_permissions_permission_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.role_has_permissions
    ADD CONSTRAINT role_has_permissions_permission_id_foreign FOREIGN KEY (permission_id) REFERENCES public.permissions(id) ON DELETE CASCADE;


--
-- Name: role_has_permissions role_has_permissions_role_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.role_has_permissions
    ADD CONSTRAINT role_has_permissions_role_id_foreign FOREIGN KEY (role_id) REFERENCES public.roles(id) ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--


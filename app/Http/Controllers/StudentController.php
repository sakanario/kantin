<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Student;
use Illuminate\Support\Facades\File;
use Faker\Generator;
use Illuminate\Container\Container;

class StudentController extends Controller
{
    public function index()
    {
        $data = Student::orderBy('student_name')->paginate(10);

        return view('admin.student', compact(['data']));
    }
}

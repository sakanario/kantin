<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Student;
use  Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class CustomRegisterController extends Controller
{
    public function index()
    {
        return view('auth.register');
    }

    public function handle()
    {
        request()->validate([
            'student_id' => ['required', 'string', 'max:255', 'exists:students,student_id'],
            'email' => ['required', 'email', 'max:255'],
            'password' => ['required', 'string', 'min:8', 'confirmed']
        ]);

        $student = Student::where('student_id', '=', request('student_id'))->first();

        // dd($student->student_id);
        
        if ($student != null) {
            // execute if student id not used
            if(is_null($student->register_status)){
                
                $user =  User::create([
                    'name' => $student->student_name,
                    'email' => request('email'),
                    'password' => Hash::make(request('password')),
                    'student_id' => $student->student_id,
                ]);
    
                $student->register_status = "registered";
                $student->save();
    
                $user->assignRole('user');
                
                Auth::login($user);

                return redirect()->route('product.index');

            }else{
                return redirect()->route('register')
                    ->with('error', 'Sorry, Student ID has been used.');
            }
        }
    }
}

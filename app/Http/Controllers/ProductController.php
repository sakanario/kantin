<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use Illuminate\Support\Facades\File;
use Faker\Generator;
use Illuminate\Container\Container;

class ProductController extends Controller
{
    public function index($sort_by = "")
    {
        $data = [];
        
        if ($sort_by == ""){
            $data = Product::latest()->paginate(20);
        }elseif ($sort_by == "product-name-asc") {
            $data = Product::orderBy('product_name', 'ASC')->paginate(20);
        }elseif ($sort_by == "product-name-desc") {
            $data = Product::orderBy('product_name', 'DESC')->paginate(20);
        }elseif ($sort_by == "latest") {
            $data = Product::latest()->paginate(20);
        }elseif ($sort_by == "oldest") {
            $data = Product::oldest()->paginate(20);
        }

        return view('home', compact(['data']));
    }

    public function store(Request $request)
    {



        // dd($input);
        // dd($_FILES);

        $request->validate([
            'product_name' => 'required',
            'product_price' => 'required',
            'product_description' => 'required',

            'product_image' => 'mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $input = $request->all();

        // saving image
        if ($image = $request->file('product_image')) {
            $destinationPath = 'image/';
            $imageName = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $imageName);
            $input['product_image'] = "$imageName";
        }

        Product::create($input);

        return redirect()->route('product.index')
            ->with('success', 'Produk berhasil ditambahkan.');
    }

    public function destroy(Product $product)
    {
        // dd($product);

        // dd('image/' . $product->product_image);
        $product->delete();

        $image_path = 'image/' . $product->product_image;  // Value is not URL but directory file path
        if (File::exists($image_path)) {
            // dd("ada");
            File::delete($image_path);
        }

        return redirect()->route('product.index')
            ->with('success', 'Terimakasih telah membeli.');
    }

    // public function test()
    // {
    //     // create faker instance
    //     $faker = Container::getInstance()->make(Generator::class);
        
    //     // generate 3 random digit number, and turn it into array 
    //     $three_num = $faker->randomNumber($nbDigits = 3, $strict = true);
    //     $three_num = strval($three_num);
    //     $three_array = str_split($three_num);

    //     // sum up the three number
    //     $three_sum = 0;
    //     foreach ($three_array as $item) {
    //         $three_sum += (int) $item;
    //     }
    //     $three_sum = (string) $three_sum;

    //     echo $three_num;
    //     echo '<br>';
    //     echo $three_sum;
    //     echo '<br>';
        
    //     // add zero if the sum number only have one digit
    //     if(strlen($three_sum) == 1 ){
    //         $three_sum = '0' . $three_sum;
    //     }

    //     // concate the three random digit with the sum digit
    //     $student_id = $three_num . $three_sum;

    //     return $student_id;
        
    // }
}

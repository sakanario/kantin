<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Celengan;


class CelenganController extends Controller
{
    public function index()
    {
        $data = Celengan::get()->first();

        // dd($data);
        
        return view('celengan', compact(['data']));
    }

    public function addMoney(Request $request)
    {
        $request->validate([
            'saldo' => 'required',
        ]);

        $input = $request->all();

        // dd($input['saldo']);

        $bank = Celengan::get()->first();

        $bank->saldo = $input['saldo'] + $bank->saldo;

        $bank->save();

        return redirect()->route('celengan.index')
            ->with('success', 'Berhasil menambahkan uang.');
    }

    public function takeMoney(Request $request)
    {
        $request->validate([
            'saldo' => 'required',
        ]);

        $input = $request->all();

        // dd($input['saldo']);

        $bank = Celengan::get()->first();

        if ($input['saldo'] > $bank->saldo){
            return redirect()->route('celengan.index')
            ->with('error', 'Maaf, saldo tidak mencukupi.');
        }

        $bank->saldo = $bank->saldo - $input['saldo'] ;

        $bank->save();

        return redirect()->route('celengan.index')
            ->with('success', 'Berhasil mengambil uang.');
    }
}

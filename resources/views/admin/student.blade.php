@extends('dashboard.base')

@section('content')
    <div class="container-fluid">
        <div class="fade-in">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Student List</h4>
                        </div>
                        <div class="card-body table-responsive">
                            <table class="table table-responsive-sm table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Student ID</th>
                                        <th>Name</th>
                                        <th>Register Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($data as $item)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $item->student_id }}</td>
                                            <td>{{ $item->student_name }}</td>
                                            @if (is_null($item->register_status))
                                                <td> <button class="btn btn-sm btn-secondary">not registered</button> </td>
                                            @else
                                                <td> <button class="btn btn-sm btn-success">registered</button> </td>
                                            @endif
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="card-footer text-muted">
                                <div class="d-flex justify-content-center">
                                    {{ $data->links() }}

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="#" method="post">
                    @csrf
                    <input hidden name="pesanan_id" type="text" value='300'>

                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Resi</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            {{-- <label for="name">Resi</label> --}}
                            <input name="resi" type="text" class="form-control" placeholder="Masukkan No. Resi">
                            {{-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> --}}
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script src="{{ asset('js/Chart.min.js') }}"></script>
    <script src="{{ asset('js/coreui-chartjs.bundle.js') }}"></script>
    <script src="{{ asset('js/main.js') }}" defer></script>

    <script>
        const showResiForm = (pesanan_id) => {
            $('input[name=pesanan_id]').attr('value', pesanan_id)
        }
    </script>
@endsection

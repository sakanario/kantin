@extends('layouts.main')

@section('content')
    <style>
        .overlay-div {
            height: 100%;
            width: 100%;
            position: absolute;
            background-color: #000;
            opacity: 0.5;
        }
    </style>
    <div class="container" style="min-height: 400px;">

        <div class="card my-5 shadow mx-auto" style="width:60%;">
            <div class="card-body">

                {{-- carausel start --}}
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                
                    <div class="carousel-inner">
                        <div class="carousel-item active">

                            <div class="overlay-div rounded"></div>
                            <img style="max-height: 200px; object-fit:cover;" class="d-block rounded w-100"
                                src="https://media.istockphoto.com/photos/white-piggy-bank-on-yellow-picture-id841053770?b=1&k=20&m=841053770&s=612x612&w=0&h=r_EfLl1jmi5djX2DCxjLHyT5vHquLXmSOQzp4PgXTus="
                                alt="First slide">
                            <div style="bottom:1px;!important" class="carousel-caption d-none d-md-block">
                                 <h3 class="card-title text-center">Celengan Kantin</h3>

                                <h1>Rp. {{number_format($data->saldo ,2,',','.') }}</h1>
                                <p>
                                    Total Saldo
                                </p>
                            </div>
                        </div>
                    </div>


                </div>
                {{-- Carausel End --}}
              <hr>
                <div class="row">

                    <div class="col">
                        <div class="d-flex">
                            
                            
                            <button class="btn btn-primary ml-auto" data-toggle="modal" data-target="#tambah-uang"> <i
                                    class="c-icon c-icon-sm cil-plus"></i> Masukan Uang</button>
                            <button class="btn btn-danger ml-2" data-toggle="modal" data-target="#ambil-uang"> <i
                                    class="c-icon c-icon-sm cil-minus"></i> Ambil Uang</button>
                        </div>
                    </div>

                </div>
                {{-- <h6 class="card-subtitle mb-2 text-muted">Card subtitle</h6>
              <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
              <a href="#" class="card-link">Card link</a>
              <a href="#" class="card-link">Another link</a> --}}
            </div>
        </div>

        {{-- Button Row --}}
        {{-- <div class="row">
            <div class="col-sm-7 my-auto">
                <p>Menampilkan 10 dari 100 product</p>
            </div>
            <div class="col-sm-3">
                <div class="d-flex my-auto">
                    <span
                        style="
                    display: inline-flex;
                    align-items: center;
                    ">
                        <b>Urutkan:</b></span>

                    <select class="form-control ml-2" aria-label="Default select example">
                        <option selected>Open this select menu</option>
                        <option value="1">One</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-2">
                <button class="btn btn-primary w-100" data-toggle="modal" data-target="#tambah-product"> <i class="c-icon c-icon-sm cil-plus"></i> Tambah Product</button>
            </div>
        </div> --}}


    </div>

    <!-- Modal Masukkan Uang-->
    <div class="modal fade" id="tambah-uang" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form action="{{ route('celengan.add') }}" method="post">
                @csrf
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Masukan Uang</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
    
                        <div class="form-group">
                            {{-- <label for="name">Harga Produk</label> --}}
                            <input name="saldo" type="number" class="form-control" placeholder="Masukan nominal uang">
                            {{-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> --}}
                        </div>
    
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Masukkan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <!-- Modal Ambil Uang -->
    <div class="modal fade" id="ambil-uang" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form action="{{ route('celengan.take') }}" method="post">
                @csrf
                <div class="modal-content">
                    
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Ambil Uang</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
    
                        <div class="form-group">
                            {{-- <label for="name">Harga Produk</label> --}}
                            <input name="saldo" type="number" class="form-control" placeholder="Masukan nominal uang">
                            {{-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> --}}
                        </div>
    
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Ambil Uang</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

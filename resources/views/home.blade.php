@extends('layouts.main')

@section('content')
    <style>
        .overlay-div {
            height: 100%;
            width: 100%;
            position: absolute;
            background-color: #000;
            opacity: 0.5;
        }


        @media only screen and (max-width: 442px) {
   
            
        }

        @media only screen and (max-width: 768px) {
          

            .carousel-caption{
                bottom: -10px;!important
            }
        }


        @media only screen and (max-width: 990px) {
            .desktop-text{
                display: none;
            }
        }

    </style>
    <div class="container" style="min-height: 500px;">
        {{-- carausel start --}}
        <div id="carouselExampleIndicators" class="carousel slide my-3" data-ride="carousel">
            {{-- <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol> --}}
            <div class="carousel-inner">
                <div class="carousel-item active">

                    {{-- <div class="overlay-div rounded"></div> --}}
                    <img style="max-height: 182px; object-fit:cover;" class="d-block rounded w-100"
                        src="{{ asset('image/food-tree.jpg') }}" alt="First slide">
                    <div class="carousel-caption d-md-block" style="color: black;text-align:left; left:5%;">
                        <h1>Kantin Kejujuran</h1>
                        <p>
                            Integrity is not something you show others. It is how you behave behind their back.
                        </p>
                    </div>
                </div>
            </div>


        </div>
        {{-- Carausel End --}}

        {{-- <div class="row">
            <div class="col-sm-7 my-auto">
                <p>Menampilkan 10 dari 100 product</p>
            </div>
            <div class="col-sm-5">
                
            </div>
            
        </div> --}}

        <div class="d-flex flex-row-reverse">
            <div class="d-flex my-auto">
                <span
                    style="
                display: inline-flex;
                align-items: center;
                ">
                    <b>Urutkan:</b></span>

                {{-- <select class="form-control ml-2" aria-label="Default select example">
                    <option disabled selected> -- Urut berdasarkan -- </option>
                    <option value="1">Nama Produk Aksending</option>
                    <option value="2">Nama Produk Descending</option>
                    <option value="3"><a href="{{ route('product.index','latest') }}">Terbaru</a></option>
                    <option value="3"><a href="">Terlama</a></option>
                </select> --}}
                <div class="dropdown w-100 ml-2">
                    <button class="w-100 btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        -- Urut berdasarkan --
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="{{ route('product.index', 'latest') }}">Produk Terbaru</a>
                        <a class="dropdown-item" href="{{ route('product.index', 'oldest') }}">Produk Terlama</a>
                        <a class="dropdown-item" href="{{ route('product.index', 'product-name-asc') }}">Nama Product
                            Ascending</a>
                        <a class="dropdown-item" href="{{ route('product.index', 'product-name-desc') }}">Nama Product
                            Descending</a>
                    </div>
                </div>
                <button class="btn btn-primary w-100 ml-2" data-toggle="modal" data-target="#tambah-product"> <i
                    class="c-icon c-icon-sm cil-plus"></i> <span class="desktop-text"> Tambah Product</span></button>
            </div>
        </div>


        {{-- Catalog Start --}}
        <div class="row mt-3">
            @foreach ($data as $item)
                {{-- item start --}}
                <div class="col-md-3">
                    <div class="card">
                        @if (empty($item->product_image))
                            <img class="card-img-top" style="min-height: 200px; max-height: 200px;"
                                src="https://sultranow.com/wp-content/uploads/2021/02/kue-bolu-panggang-500x375.jpg"
                                alt="Card image cap">
                        @else
                            <img class="card-img-top" style="min-height: 200px; max-height: 200px;"
                                src="{{ asset('image/' . $item->product_image) }}" alt="Card image cap">
                        @endif
                        <div class="card-body">
                            <h5 class="card-title">{{ $item->product_name }}</h5>
                            <p data-coreui-toggle="tooltip" data-coreui-placement="top"
                                title="{{ $item->product_description }}" class="card-text">
                                {{ \Illuminate\Support\Str::limit($item->product_description, 80, $end = '...') }}</p>
                            <p class="card-text">Rp. {{ number_format($item->product_price, 2, ',', '.') }}</p>
                            <p class="card-text">Dibuat : {{ $item->created_at }}</p>
                            <button type="button" onclick="confirmBuy('{{ route('product.delete', $item->id) }}')"
                                class="btn btn-primary btn-sm w-100" data-toggle="modal"
                                data-target="#confirm-buy">Beli</button>


                        </div>
                    </div>
                </div>
                {{-- item end --}}
            @endforeach


        </div>
        <div class="d-flex justify-content-center">
            {{ $data->links() }}

        </div>
        {{-- Catalog End --}}
    </div>

    <!-- Tambah Product Modal -->
    <div class="modal fade" id="tambah-product" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form action="{{ route('product.store') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Tambah Produk</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="name">Nama Produk</label>
                            <input name="product_name" type="text" class="form-control" placeholder="Ketik Nama Product">
                            {{-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> --}}
                        </div>

                        <div class="form-group">
                            <label for="name">Harga Produk</label>
                            <input name="product_price" type="number" class="form-control"
                                placeholder="Ketik Harga Product">
                            {{-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> --}}
                        </div>

                        <div class="form-group">
                            <label for="alamat">Deskripsi Produk</label>
                            <textarea name="product_description" class="form-control" rows="3"></textarea>
                        </div>

                        <div class="form-group">
                            <label for="product_image">Unggah Gambar</label>
                            <input required name="product_image" type="file" class="form-control-file">
                            {{-- <small>Silahkan upload bukti pembayaran sesuai nominal yang tertera.</small> --}}
                        </div>



                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </div>

            </form>
        </div>
    </div>

    <!-- Buy Confirmation Modal -->
    <div class="modal fade" id="confirm-buy" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">

            <form id="confirm-buy-form" action="#" method="post">
                @method('DELETE')
                @csrf
                <div class="modal-content">
                    <div class="modal-header" style="display: block; !important">
                        <h5 class="modal-title text-center" id="confirm-buy-title">Beneran mau beli nih?</h5>
                        {{-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button> --}}
                    </div>
                    <div class="modal-body">
                        <h6>Nanti jangan lupa masukan uang ke <a href="{{ route('celengan.index') }}">money box</a> sesuai
                            harga barang yang kamu beli ya!</h6>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Ga jadi deh</button>
                        <button type="submit" class="btn btn-primary">Ya!</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
@endsection

@section('javascript')
    <script>
        const confirmBuy = (form_route) => {
            $('#confirm-buy-form').attr('action', form_route);
        }
    </script>
@endsection
